package servlets;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controllers.DepartmentController;
import controllers.EmployeeController;
import controllers.GradingController;
import controllers.LoginController;
import controllers.ReimbursementController;

public class RequestHelper {
	public static void process(HttpServletRequest request, HttpServletResponse response) throws IOException {

		StringBuffer url = request.getRequestURL();
		String uri = request.getRequestURI();
		System.out.println("URL: " + url + "\nURI: " + uri);

		/*
		 * this switch handle various requests
		 */
		switch (uri) {

		// Department

		case "/TRMS/getDepartment.do": {
			DepartmentController.getDepartment(request, response);
			break;
		}

		case "/TRMS/getAllDepartments.do": {
			DepartmentController.getAllDepartments(request, response);
			break;
		}
		case "/TRMS/addDepartment.do": {
			DepartmentController.addDepartment(request, response);
			break;
		}
		case "/TRMS/updateDepartment.do": {
			DepartmentController.updateDepartment(request, response);
			break;
		}
		case "/TRMS/deleteDepartment.do": {
			DepartmentController.deleteDepartment(request, response);
			break;
		}
		// Reimbursement

		case "/TRMS/getReimbursement.do": {
			ReimbursementController.getReimbursement(request, response);
			break;
		}

		case "/TRMS/getAllReimbursements.do": {
			ReimbursementController.getAllReimbursements(request, response);
			break;
		}
		case "/TRMS/addReimbursement.do": {
			ReimbursementController.addReimbursement(request, response);
			break;
		}
		case "/TRMS/updateReimbursement.do": {
			ReimbursementController.updateReimbursement(request, response);
			break;
		}
		case "/TRMS/deleteReimbursement.do": {
			ReimbursementController.deleteReimbursement(request, response);
			break;
		}

		// Employee

		case "/TRMS/getEmployee.do": {
			EmployeeController.getEmployee(request, response);
			break;
		}

		case "/TRMS/getAllEmployees.do": {
			EmployeeController.getAllEmployees(request, response);
			break;
		}
		case "/TRMS/addEmployee.do": {
			EmployeeController.addEmployee(request, response);
			break;
		}
		case "/TRMS/updateEmployee.do": {
			EmployeeController.updateEmployee(request, response);
			break;
		}
		case "/TRMS/deleteEmployee.do": {
			EmployeeController.deleteEmployee(request, response);
			break;
		}

		// Grading
		case "/TRMS/getGrading.do": {
			GradingController.getGrading(request, response);
			break;
		}

		case "/TRMS/getAllGrading.do": {
			GradingController.getAllGrading(request, response);
			break;
		}
		case "/TRMS/addGrading.do": {
			GradingController.addGrading(request, response);
			break;
		}
		case "/TRMS/updateGrading.do": {
			GradingController.updateGrading(request, response);
			break;
		}
		case "/TRMS/deleteGrading.do": {
			GradingController.deleteGrading(request, response);
			break;
		}

		// Login
		case "/TRMS/getLogin.do": {
			LoginController.getLogin(request, response);
			break;
		}

		case "/TRMS/getAllLogin.do": {
			LoginController.getAllLogin(request, response);
			break;
		}
		case "/TRMS/addLogin.do": {
			LoginController.addLogin(request, response);
			break;
		}
		case "/TRMS/updateLogin.do": {
			LoginController.updateLogin(request, response);
			break;
		}
		case "/TRMS/deleteLogin.do": {
			LoginController.deleteLogin(request, response);
			break;
		}

		// Approve an event
		case "/TRMS/event/approve.do": {
			ReimbursementController.approveEvent(request, response);
			break;
		}

		// Deny an event
		case "/TRMS/event/deny.do": {
			ReimbursementController.denyEvent(request, response);
			break;
		}

		// Do Login
		case "/TRMS/Login.do": {
			LoginController.doLogin(request, response);
			break;
		}

		// Request by Department
		case "/TRMS/RequestsByDepartment.do": {
			ReimbursementController.RequestsByDepartment(request, response);
			break;
		}

		// Request by Supervisor
		case "/TRMS/RequestsBySupervisor.do": {
			ReimbursementController.RequestsBySupervisor(request, response);
			break;
		}
		
		default: {
			response.sendError(451, "Get off my lawn!");
			break;
		}
		}
	}
}
