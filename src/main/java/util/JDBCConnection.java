package util;

import java.sql.Connection;
import java.sql.DriverManager;

import services.DepartmentService;

public class JDBCConnection {
private static Connection conn = null;
	
	public static Connection getConnection() {
		
		//If Connection isn't created, create one.
		//If Connection is created, return that created object.
		
		try {
			
			if (conn == null) {
				//Create a new Connection
				
				//To establish a connection, we need 3 credentials:
				//username, password, url (endpoint)
				Class.forName("oracle.jdbc.driver.OracleDriver");
				String username = "raidis";
				String password = "Testing1234";
				String endpoint = "usf2011bootcamp.cwen3yeqhixc.us-east-2.rds.amazonaws.com";
				String url = "jdbc:oracle:thin:@" + endpoint + ":1521:ORCL";
				
				conn = DriverManager.getConnection(url, username, password);
				
				return conn;
			} else {
				//If already created, return conn
				return conn;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/*
	 * This is for testing purposes ONLY. Not Need to use JDBC
	 */
	public static void main(String[] args) {
		
		Connection conn1 = getConnection();
		Connection conn2 = getConnection();
		
		System.out.println(conn1);
		System.out.println(conn2);
		
		DepartmentService ds = new DepartmentService();
		System.out.println(ds.getDepartment(1));
		
	}

}
