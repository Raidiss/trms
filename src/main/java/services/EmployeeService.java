package services;

import java.util.List;

import dao.EmployeeDAO;
import dao.EmployeeDAOImpl;
import models.Employee;

public class EmployeeService {
	    public EmployeeDAO ed = new EmployeeDAOImpl();
	    
	    public Employee getEmployee(int id) {
	        return ed.getEmployee(id);
	            }
	    public Employee addEmployee(Employee em) {
	        return ed.addEmployee(em);
	    }
	    public List<Employee> getAllEmployees(){
	        return ed.getAllEmployees(); 
	    }
	    public boolean updateEmployee(Employee change) {
	    	return ed.updateEmployee(change); 
	        
	    }
		public boolean deleteEmployee(int id) {
			return ed.deleteEmployee(id); 
		}
	    
	    
	}
