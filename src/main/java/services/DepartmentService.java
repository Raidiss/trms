package services;

import java.util.List;

import dao.DepartmentDAO;
import dao.DepartmentDAOImpl;
import models.Department;

public class DepartmentService {
	public DepartmentDAO dd = new DepartmentDAOImpl();
	
	public Department getDepartment(int id) {
		return dd.getDepartment(id);
	}
	
	public Department addDepartment(Department d) {
		return dd.addDepartment(d);
	}
	
	public List<Department> getAllDepartments() {
		return dd.getAllDepartments();
	}
	public boolean updateDepartment(Department change) {
		return dd.updateDepartment(change);
	}
	public boolean deleteDepartment(int id) {
		return dd.deleteDepartment(id);
	}
}
