package services;

import java.util.List;

import dao.LoginDAO;
import dao.LoginDAOImpl;
import models.Employee;
import models.Login;

public class LoginService {
public LoginDAO ld = new LoginDAOImpl();
	
	public Login getLogin(int id) {
		return ld.getLogin(id);
	}
	
	public Login addLogin(Login l) {
		return ld.addLogin(l);
	}
	
	public List<Login> getAllLogin() {
		return ld.getAllLogin();
	}

	public boolean updateLogin(Login change) {
		return ld.updateLogin(change);
		}

	public boolean deleteLogin(int id) {
		return ld.deleteLogin(id);
	}
	
	public Employee doLogin(String username, String password) {
		return ld.doLogin(username, password);
	}

}
