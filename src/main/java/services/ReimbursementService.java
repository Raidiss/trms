package services;

import java.util.List;

import dao.ReimbursementDAO;
import dao.ReimbursementDAOImpl;
import models.Reimbursement;


public class ReimbursementService {
	public ReimbursementDAO rd = new ReimbursementDAOImpl();
	
	public Reimbursement getReimbursement(int id) {
		return rd.getReimbursement(id);
	}
	
	public Reimbursement addReimbursement(Reimbursement r) {
		return rd.addReimbursement(r);
	}
	public boolean updateReimbursement(Reimbursement change) {
		return rd.updateReimbursement(change);
	}
	
	public List<Reimbursement> getAllReimbursements() {
		return rd.getAllReimbursements();
	}
	
	public boolean deleteReimbursement(int id) {
		return rd.deleteReimbursement(id);
	}
	
	public boolean eventApprove(int id, int empId) {
		return rd.eventApprove(id, empId);
	}
	
	public boolean eventDeny(int id, int empId) {
		return rd.eventDeny(id, empId);
	}

	public List<Reimbursement> RequestsByDepartment(int departmentId) {
		return rd.RequestsByDepartment(departmentId);
	}
	public List<Reimbursement> RequestsBySupervisor(int supervisorId) {
		return rd.RequestsByDepartment(supervisorId);
	}
}
