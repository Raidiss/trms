package services;

import java.util.List;

import dao.GradingDAO;
import dao.GradingDAOImpl;
import models.Grading;

public class GradingService {
	public GradingDAO gd = new GradingDAOImpl();
	
	public Grading getGrading(int id) {
		return gd.getGrading(id);
	}
	
	public Grading addGrading(Grading g) {
		return gd.addGrading(g);
	}
	
	public List<Grading> getAllGrading() {
		return gd.getAllGrading();
	}

	public boolean updateGrading(Grading change) {
		return gd.updateGrading(change);
		
	}

	public boolean deleteGrading(int id) {
		return gd.deleteGrading(id);

	}
}
