package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import models.Employee;
import services.EmployeeService;

public class EmployeeController {
	public static EmployeeService es = new EmployeeService();
	public static Gson gson = new Gson();
	
	public static void getEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String input = request.getParameter("id");

		int id;
		try {
			id = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			id = -1;
		}

		Employee e = es.getEmployee(id);

		response.getWriter().append(e.toString());
		}
	
	public static void getAllEmployees(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		List<Employee> employeeList = es.getAllEmployees();
		
		response.getWriter().append(gson.toJson(employeeList));
		
	}
	
	public static void addEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Employee employee = gson.fromJson(request.getReader(), Employee.class);
		
		//add Employee
		Employee newEmployee = es.addEmployee(employee);
		//generate a response
		response.getWriter().append(gson.toJson(newEmployee));
		
	}
	
	public static void updateEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		//Processing the request
		Employee change = gson.fromJson(request.getReader(), Employee.class);
		
		//call a service
		es.updateEmployee(change);
		
		//Generate a response
		response.getWriter().append(gson.toJson(change));
	}
	
	public static void deleteEmployee(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String[]> paramMap = request.getParameterMap();
		if(paramMap.containsKey("id")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean worked = es.deleteEmployee(id);
			response.getWriter().append(Boolean.toString(worked));
		} else {
			response.getWriter().append(Boolean.toString(false));
		}
	}

}
