package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import models.Department;
import services.DepartmentService;

public class DepartmentController {
	public static DepartmentService ds = new DepartmentService();
	public static Gson gson = new Gson();


	public static void getDepartment(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String input = request.getParameter("id");

		int id;
		try {
			id = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			id = -1;
		}

		Department d = ds.getDepartment(id);

		response.getWriter().append(d.toString());
		}
	
	public static void getAllDepartments(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		List<Department> departments = ds.getAllDepartments();
		
		response.getWriter().append(gson.toJson(departments));
		
	}
	
	public static void addDepartment(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Department department = gson.fromJson(request.getReader(), Department.class);
		
		//add department
		Department newDepa = ds.addDepartment(department);
		//generate a response
		response.getWriter().append(gson.toJson(newDepa));
		
	}
	
	public static void updateDepartment(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		//Processing the request
		Department change = gson.fromJson(request.getReader(), Department.class);
		
		//call a service
		ds.updateDepartment(change);
		
		//Generate a response
		response.getWriter().append(gson.toJson(change));
	}
	
	public static void deleteDepartment(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String[]> paramMap = request.getParameterMap();
		if(paramMap.containsKey("id")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean worked = ds.deleteDepartment(id);
			response.getWriter().append(Boolean.toString(worked));
		} else {
			response.getWriter().append(Boolean.toString(false));
		}
	}
}

