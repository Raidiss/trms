package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import models.Reimbursement;
import services.ReimbursementService;

public class ReimbursementController {
	public static ReimbursementService rs = new ReimbursementService();
	public static Gson gson = new Gson();

	public static void getReimbursement(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// http://localhost:8080/TRMS/getDepartment.do?id=1
		String input = request.getParameter("id");

		int id;
		try {
			id = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			id = -1;
		}

		Reimbursement r = rs.getReimbursement(id);

		response.getWriter().append(gson.toJson(r));
	}

	public static void getAllReimbursements(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		List<Reimbursement> reimbursements = rs.getAllReimbursements();

		response.getWriter().append(gson.toJson(reimbursements));

	}

	public static void addReimbursement(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Reimbursement reimbursement = gson.fromJson(request.getReader(), Reimbursement.class);

		// add Reimbursement
		Reimbursement newReim = rs.addReimbursement(reimbursement);
		// generate a response
		response.getWriter().append(gson.toJson(newReim));

	}

	public static void updateReimbursement(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		// Processing the request
		Reimbursement change = gson.fromJson(request.getReader(), Reimbursement.class);

		// call a service
		rs.updateReimbursement(change);

		// Generate a response
		response.getWriter().append(gson.toJson(change));
	}

	public static void deleteReimbursement(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, String[]> paramMap = request.getParameterMap();
		if (paramMap.containsKey("id")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean worked = rs.deleteReimbursement(id);
			response.getWriter().append(Boolean.toString(worked));
		} else {
			response.getWriter().append(Boolean.toString(false));
		}
	}

	public static void approveEvent(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String input1 = request.getParameter("id");
		String input2 = request.getParameter("empId");
		int id;
		int empId;
		try {
			id = Integer.parseInt(input1);
			empId = Integer.parseInt(input2);
		} catch (NumberFormatException e) {
			id = -1;
			empId = -1;
		}
		rs.eventApprove(id, empId);

		// TODO send correct response later.
		response.getWriter().append("");

	}

	public static void denyEvent(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String input1 = request.getParameter("id");
		String input2 = request.getParameter("empId");
		int id;
		int empId;
		try {
			id = Integer.parseInt(input1);
			empId = Integer.parseInt(input2);
		} catch (NumberFormatException e) {
			id = -1;
			empId = -1;
		}
		rs.eventDeny(id, empId);

		// TODO send correct response later.
		response.getWriter().append("");

	}

	public static void RequestsByDepartment(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// Processing the request
//		Employee requests = gson.fromJson(request.getReader(), Employee.class);
		String param = request.getParameter("departmentId");
		int departmentId;
		try {
			departmentId = Integer.parseInt(param);
		} catch (NumberFormatException e) {
			departmentId = -1;
		}

		// call a service
		List<Reimbursement> rems = rs.RequestsByDepartment(departmentId);
		// Generate a response
		response.getWriter().append(gson.toJson(rems));

	}

	public static void RequestsBySupervisor(HttpServletRequest request, HttpServletResponse response)
		throws IOException {
			// Processing the request
//			Employee requests = gson.fromJson(request.getReader(), Employee.class);
			String param = request.getParameter("supervisorId");
			int supervisorId;
			try {
				supervisorId = Integer.parseInt(param);
			} catch (NumberFormatException e) {
				supervisorId = -1;
			}

			// call a service
			List<Reimbursement> rems = rs.RequestsByDepartment(supervisorId);
			// Generate a response
			response.getWriter().append(gson.toJson(rems));

		}
}
