package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import models.Grading;
import services.GradingService;

public class GradingController {
	public static GradingService gs = new GradingService();
	public static Gson gson = new Gson();


	public static void getGrading(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String input = request.getParameter("id");

		int id;
		try {
			id = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			id = -1;
		}

		Grading g = gs.getGrading(id);

		response.getWriter().append(g.toString());
	}
	
public static void getAllGrading(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		List<Grading> gradingList = gs.getAllGrading();
		
		response.getWriter().append(gson.toJson(gradingList));
		
	}

public static void addGrading(HttpServletRequest request, HttpServletResponse response) throws IOException {
	Grading grading = gson.fromJson(request.getReader(), Grading.class);
	
	//add Grading
	Grading newGrade = gs.addGrading(grading);
	//generate a response
	response.getWriter().append(gson.toJson(newGrade));
	
}

public static void updateGrading(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
	
	//Processing the request
	Grading change = gson.fromJson(request.getReader(), Grading.class);
	
	//call a service
	gs.updateGrading(change);
	
	//Generate a response
	response.getWriter().append(gson.toJson(change));
}

public static void deleteGrading(HttpServletRequest request, HttpServletResponse response) throws IOException {
	Map<String, String[]> paramMap = request.getParameterMap();
	if(paramMap.containsKey("id")) {
		int id = Integer.parseInt(request.getParameter("id"));
		boolean worked = gs.deleteGrading(id);
		response.getWriter().append(Boolean.toString(worked));
	} else {
		response.getWriter().append(Boolean.toString(false));
	}
}
}
