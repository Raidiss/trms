package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import models.Employee;
import models.Login;
import services.LoginService;

public class LoginController {
	public static LoginService ls = new LoginService();
	public static Gson gson = new Gson();


	public static void getLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// http://localhost:8080/TRMS/getLogin.do?id=1
		String input = request.getParameter("id");

		int id;
		try {
			id = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			id = -1;
		}

		Login l = ls.getLogin(id);

		response.getWriter().append(l.toString());
	}
	
public static void getAllLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		List<Login> loginList = ls.getAllLogin();
		
		response.getWriter().append(gson.toJson(loginList));
		
	}
public static void addLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
	Login login = gson.fromJson(request.getReader(), Login.class);
	
	//add Login
	Login newLogin = ls.addLogin(login);
	//generate a response
	response.getWriter().append(gson.toJson(newLogin));
	
}

public static void updateLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
	
	//Processing the request
	Login change = gson.fromJson(request.getReader(), Login.class);
	
	//call a service
	ls.updateLogin(change);
	
	//Generate a response
	response.getWriter().append(gson.toJson(change));
}

public static void deleteLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
	Map<String, String[]> paramMap = request.getParameterMap();
	if(paramMap.containsKey("id")) {
		int id = Integer.parseInt(request.getParameter("id"));
		boolean worked = ls.deleteLogin(id);
		response.getWriter().append(Boolean.toString(worked));
	} else {
		response.getWriter().append(Boolean.toString(false));
	}
}

public static void doLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
	//Processing the request
		Login login = gson.fromJson(request.getReader(), Login.class);
		
		//call a service
		Employee e = ls.doLogin(login.getUsername(),login.getPassword());
		
		//Generate a response
		response.getWriter().append(gson.toJson(e));

}
}