package dao;

import java.util.List;

import models.Employee;
import models.Login;

public interface LoginDAO {
	public Login getLogin (int id);
	public List<Login> getAllLogin();
	public Login addLogin(Login l);
	public boolean updateLogin(Login change);
	public boolean deleteLogin(int id);
	public Employee doLogin(String username, String password);
}
