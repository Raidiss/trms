package dao;

import java.util.List;

import models.Grading;

public interface GradingDAO {
	public Grading getGrading (int id);
	public List<Grading> getAllGrading();
	public Grading addGrading(Grading g);
	public boolean updateGrading(Grading change);
	public boolean deleteGrading(int id);
}
