package dao;

import java.util.List;

import models.Department;

public interface DepartmentDAO {
	public Department getDepartment (int id);
	public List<Department> getAllDepartments();
	public Department addDepartment(Department d);
	public boolean updateDepartment(Department change);
	public boolean deleteDepartment(int id);

}
