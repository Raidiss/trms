package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {
	public static Connection conn = util.JDBCConnection.getConnection();

	public Employee getEmployee(int id) {
		try {
			String sql = "SELECT * FROM employee WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Employee em = new Employee();
				em.setId(rs.getInt("ID"));
				em.setName(rs.getString("NAME"));
				em.setLastName(rs.getString("LAST_NAME"));
				em.setEmployeeType(rs.getString("EMPLOYEE_TYPE"));
				em.setSupervisorId(rs.getInt("SUPERVISOR_ID"));
				em.setDepartmentId(rs.getInt("DEPARTMENT_ID"));

				return em;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	private int getNextIdentifier() {
		try {
			String sql = "SELECT employee_seq.nextval FROM dual";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
		}
		return 0;
	}

	public Employee addEmployee(Employee em) {
		try {
			int id = getNextIdentifier();
			String sql = "INSERT INTO employee (id, name, last_name, employee_type, supervisor_id, department_id) VALUES (?,?,?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setString(2, em.getName());
			ps.setString(3, em.getLastName());
			ps.setString(4, em.getEmployeeType());
			ps.setInt(5, em.getSupervisorId());
			ps.setInt(6, em.getDepartmentId());

			ps.executeQuery();
			return new Employee(id, em.getName(), em.getLastName(), em.getEmployeeType(), em.getSupervisorId(), em.getDepartmentId());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}


	public List<Employee> getAllEmployees() {
		try {
			String sql = "SELECT * FROM employee";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Employee> Employee = new ArrayList<Employee>();
			while (rs.next()) {
				Employee em = new Employee();
				em.setId(rs.getInt("ID"));
				em.setName(rs.getString("NAME"));
				em.setLastName(rs.getString("LAST_NAME"));
				em.setEmployeeType(rs.getString("EMPLOYEE_TYPE"));
				em.setSupervisorId(rs.getInt("SUPERVISOR_ID"));
				em.setDepartmentId(rs.getInt("DEPARTMENT_ID"));
				Employee.add(em);

			}
			return Employee;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean deleteEmployee(int id) {
		try {

			String sql = "DELETE employee WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateEmployee(Employee change) {
		try {

			String sql = "UPDATE employee SET name = ?, last_mame = ?, employee_type = ?, supervisor_id = ?, department_id = ?  WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, change.getName());
			ps.setString(2, change.getLastName());
			ps.setString(3, change.getEmployeeType());
			ps.setInt(4, change.getSupervisorId());
			ps.setInt(5, change.getDepartmentId());
			ps.setInt(5, change.getId());
	
			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
}
	}
