package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Reimbursement;
import util.JDBCConnection;

public class ReimbursementDAOImpl implements ReimbursementDAO {
	public static Connection conn = JDBCConnection.getConnection();

	public Reimbursement getReimbursement(int id) {

		try {

			String sql = "SELECT * FROM reimbursement WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, Integer.toString(id));

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Reimbursement r = new Reimbursement();
				r.setId(rs.getInt("ID"));
				r.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				r.setGradingId(rs.getInt("GRADING_ID"));
				r.setStartDate(rs.getString("START_DATE"));
				r.setTime(rs.getString("TIME"));
				r.setLocation(rs.getString("LOCATION"));
				r.setDescription(rs.getString("DESCRIPTION"));
				r.setCost(rs.getInt("COST"));
				r.setEventType(rs.getString("EVENT_TYPE"));
				r.setWorkRelation(rs.getString("WORK_RELATION"));
				r.setApprovedBySupervisor(rs.getString("APPROVED_BY_SUPERVISOR"));
				r.setApprovedByHead(rs.getString("APPROVED_BY_HEAD"));
				r.setApprovedByBenco(rs.getString("APPROVED_BY_BENCO"));
				r.setNotes(rs.getString("NOTES"));
				return r;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<Reimbursement> getAllReimbursements() {

		try {

			String sql = "SELECT * FROM reimbursement";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

			while (rs.next()) {

				Reimbursement r = new Reimbursement();
				r.setId(rs.getInt("ID"));
				r.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				r.setGradingId(rs.getInt("GRADING_ID"));
				r.setStartDate(rs.getString("START_DATE"));
				r.setTime(rs.getString("TIME"));
				r.setLocation(rs.getString("LOCATION"));
				r.setDescription(rs.getString("DESCRIPTION"));
				r.setCost(rs.getInt("COST"));
				r.setEventType(rs.getString("EVENT_TYPE"));
				r.setWorkRelation(rs.getString("WORK_RELATION"));
				r.setApprovedBySupervisor(rs.getString("APPROVED_BY_SUPERVISOR"));
				r.setApprovedByHead(rs.getString("APPROVED_BY_HEAD"));
				r.setApprovedByBenco(rs.getString("APPROVED_BY_BENCO"));
				r.setNotes(rs.getString("NOTES"));

				reimbursements.add(r);
			}

			return reimbursements;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	private int getNextIdentifier() {
		try {
			String sql = "SELECT reimbursement_seq.nextval FROM dual";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
		}
		return 0;
	}

	public Reimbursement addReimbursement(Reimbursement r) {

		try {

			int id = getNextIdentifier();

			String sql = "INSERT INTO reimbursement (id, employee_id, grading_id, start_date, time, location, description, cost, event_type, work_relation,"
					+ " approved_by_supervisor, approved_by_head, approved_by_benco, notes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setInt(2, r.getEmployeeId());
			ps.setInt(3, r.getGradingId());
			ps.setString(4, r.getStartDate());
			ps.setString(5, r.getTime());
			ps.setString(6, r.getLocation());
			ps.setString(7, r.getDescription());
			ps.setInt(8, r.getCost());
			ps.setString(9, r.getEventType());
			ps.setString(10, r.getWorkRelation());
			ps.setString(11, r.getApprovedBySupervisor());
			ps.setString(12, r.getApprovedByHead());
			ps.setString(13, r.getApprovedByBenco());
			ps.setString(14, r.getNotes());

			ps.executeQuery();
			return new Reimbursement(id, r.getEmployeeId(), r.getGradingId(), r.getStartDate(), r.getTime(),
					r.getLocation(), r.getDescription(), r.getCost(), r.getEventType(), r.getWorkRelation(),
					r.getApprovedBySupervisor(), r.getApprovedByHead(), r.getApprovedByBenco(), r.getNotes());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean updateReimbursement(Reimbursement change) {

		try {

			String sql = "UPDATE reimbursement SET employee_id = ?, grading_id = ?, start_date = ?, time = ?, "
					+ "location = ?, description = ?, cost = ?, event_type = ?, work_relation = ?, approved_by_supervisor = ?, "
					+ "approved_by_head = ?, approved_by_benco = ?, notes =?  WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, change.getEmployeeId());
			ps.setInt(2, change.getGradingId());
			ps.setString(3, change.getStartDate());
			ps.setString(4, change.getTime());
			ps.setString(5, change.getLocation());
			ps.setString(6, change.getDescription());
			ps.setInt(7, change.getCost());
			ps.setString(8, change.getEventType());
			ps.setString(9, change.getWorkRelation());
			ps.setString(10, change.getApprovedBySupervisor());
			ps.setString(11, change.getApprovedByHead());
			ps.setString(12, change.getApprovedByBenco());
			ps.setString(13, change.getNotes());
			ps.setInt(14, change.getId());
			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteReimbursement(int id) {

		try {

			String sql = "DELETE reimbursement WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean eventApprove(int id, int empId) {
		try {
			String sql = "SELECT reimbursement.*, employee.supervisor_id, department.head_id FROM (reimbursement INNER JOIN employee ON reimbursement.employee_id = employee.id) INNER JOIN department ON "
					+ " employee.department_id = department.id WHERE reimbursement.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int supervisorId = rs.getInt("SUPERVISOR_ID");
				int headId = rs.getInt("HEAD_ID");
				if (empId == supervisorId || empId == headId) {
					String supApproved = empId == supervisorId ? "yes" : rs.getString("APPROVED_BY_SUPERVISOR");
					String headApproved = empId == headId ? "yes" : rs.getString("APPROVED_BY_HEAD");
					Reimbursement rem = new Reimbursement(rs.getInt("ID"), rs.getInt("EMPLOYEE_ID"),
							rs.getInt("GRADING_ID"), rs.getString("START_DATE"), rs.getString("TIME"),
							rs.getString("LOCATION"), rs.getString("DESCRIPTION"), rs.getInt("COST"),
							rs.getString("EVENT_TYPE"), rs.getString("WORK_RELATION"), supApproved, headApproved,
							rs.getString("APPROVED_BY_BENCO"), rs.getString("NOTES"));

					return updateReimbursement(rem);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean eventDeny(int id, int empId) {
		try {
			String sql = "SELECT reimbursement.*, employee.supervisor_id, department.head_id FROM (reimbursement INNER JOIN employee ON reimbursement.employee_id = employee.id) INNER JOIN department ON "
					+ " employee.department_id = department.id WHERE reimbursement.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int supervisorId = rs.getInt("SUPERVISOR_ID");
				int headId = rs.getInt("HEAD_ID");
				if (empId == supervisorId || empId == headId) {
					String supApproved = empId == supervisorId ? "no" : rs.getString("APPROVED_BY_SUPERVISOR");
					String headApproved = empId == headId ? "no" : rs.getString("APPROVED_BY_HEAD");
					Reimbursement rem = new Reimbursement(rs.getInt("ID"), rs.getInt("EMPLOYEE_ID"),
							rs.getInt("GRADING_ID"), rs.getString("START_DATE"), rs.getString("TIME"),
							rs.getString("LOCATION"), rs.getString("DESCRIPTION"), rs.getInt("COST"),
							rs.getString("EVENT_TYPE"), rs.getString("WORK_RELATION"), supApproved, headApproved,
							rs.getString("APPROVED_BY_BENCO"), rs.getString("NOTES"));

					return updateReimbursement(rem);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public List<Reimbursement> RequestsByDepartment(int departmentId) {
		try {
			String sql = "SELECT reimbursement.* FROM reimbursement INNER JOIN employee ON reimbursement.employee_id = employee.id"
					+ " WHERE employee.department_id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(departmentId));

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

			while (rs.next()) {
				Reimbursement r = new Reimbursement();
				r.setId(rs.getInt("ID"));
				r.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				r.setGradingId(rs.getInt("GRADING_ID"));
				r.setStartDate(rs.getString("START_DATE"));
				r.setTime(rs.getString("TIME"));
				r.setLocation(rs.getString("LOCATION"));
				r.setDescription(rs.getString("DESCRIPTION"));
				r.setCost(rs.getInt("COST"));
				r.setEventType(rs.getString("EVENT_TYPE"));
				r.setWorkRelation(rs.getString("WORK_RELATION"));
				r.setApprovedBySupervisor(rs.getString("APPROVED_BY_SUPERVISOR"));
				r.setApprovedByHead(rs.getString("APPROVED_BY_HEAD"));
				r.setApprovedByBenco(rs.getString("APPROVED_BY_BENCO"));
				r.setNotes(rs.getString("NOTES"));

				reimbursements.add(r);
			}

			return reimbursements;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new ArrayList<Reimbursement>(0);	
 }

	@Override
	public List<Reimbursement> RequestsBySupervisor(int supervisorId) {
		try {
			String sql = "SELECT reimbursement.* FROM reimbursement INNER JOIN employee ON reimbursement.employee_id = employee.id"
					+ " WHERE employee.supervisor_id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(supervisorId));

			ResultSet rs = ps.executeQuery();

			List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

			while (rs.next()) {
				Reimbursement r = new Reimbursement();
				r.setId(rs.getInt("ID"));
				r.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				r.setGradingId(rs.getInt("GRADING_ID"));
				r.setStartDate(rs.getString("START_DATE"));
				r.setTime(rs.getString("TIME"));
				r.setLocation(rs.getString("LOCATION"));
				r.setDescription(rs.getString("DESCRIPTION"));
				r.setCost(rs.getInt("COST"));
				r.setEventType(rs.getString("EVENT_TYPE"));
				r.setWorkRelation(rs.getString("WORK_RELATION"));
				r.setApprovedBySupervisor(rs.getString("APPROVED_BY_SUPERVISOR"));
				r.setApprovedByHead(rs.getString("APPROVED_BY_HEAD"));
				r.setApprovedByBenco(rs.getString("APPROVED_BY_BENCO"));
				r.setNotes(rs.getString("NOTES"));

				reimbursements.add(r);
			}

			return reimbursements;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new ArrayList<Reimbursement>(0);	
	}
}