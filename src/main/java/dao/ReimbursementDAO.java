package dao;

import java.util.List;

import models.Reimbursement;


public interface ReimbursementDAO {
	public Reimbursement getReimbursement(int id);
	public List<Reimbursement> getAllReimbursements();
	public Reimbursement addReimbursement(Reimbursement r);
	public boolean updateReimbursement(Reimbursement change);
	public boolean deleteReimbursement(int id);
	public boolean eventApprove(int id, int empId);
	public boolean eventDeny(int id, int empId);
	public List<Reimbursement> RequestsByDepartment(int departmentId);
	public List<Reimbursement> RequestsBySupervisor(int supervisorId);

}
