package dao;

import java.util.List;

import models.Employee;

public interface EmployeeDAO {
    
    public Employee getEmployee(int id);
    public Employee addEmployee(Employee em);
    public List<Employee> getAllEmployees();
    public boolean deleteEmployee(int id);
    public boolean updateEmployee(Employee change);
}
