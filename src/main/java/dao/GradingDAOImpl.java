package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Grading;
import util.JDBCConnection;

public class GradingDAOImpl implements GradingDAO {

	public static Connection conn = JDBCConnection.getConnection();

	public Grading getGrading(int id) {
		try {

			String sql = "SELECT * FROM grading WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, Integer.toString(id));

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Grading g = new Grading();
				g.setId(rs.getInt("ID"));
				g.setValue(rs.getString("VALUE"));

				return g;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<Grading> getAllGrading() {
		try {

			String sql = "SELECT * FROM grading";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Grading> grades = new ArrayList<Grading>();

			while (rs.next()) {

				Grading d = new Grading();
				d.setId(rs.getInt("ID"));
				d.setValue(rs.getString("VALUE"));

				grades.add(d);
			}

			return grades;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	private int getNextIdentifier() {
		try {
			String sql = "SELECT department_seq.nextval FROM dual";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
		}
		return 0;
	}

	public Grading addGrading(Grading g) {
		try {
			int id = getNextIdentifier();
			String sql = "INSERT INTO grading (id, value) VALUES (?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setString(2, g.getValue());

			ps.executeQuery();
			return new Grading(id, g.getValue());


		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean updateGrading(Grading change) {
		try {

			String sql = "UPDATE grading SET value = ? WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, change.getValue());
			ps.setInt(2, change.getId());

			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteGrading(int id) {
		try {

			String sql = "DELETE grading WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}
