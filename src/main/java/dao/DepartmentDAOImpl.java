package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Department;
import util.JDBCConnection;

public class DepartmentDAOImpl implements DepartmentDAO {
	public static Connection conn = JDBCConnection.getConnection();

	public Department getDepartment(int id) {

		try {
			
			String sql = "SELECT * FROM department WHERE id = ?";
		
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, Integer.toString(id));

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				
				Department d = new Department(rs.getInt("ID"), rs.getString("NAME"), rs.getInt("HEAD_ID"));
				return d;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<Department> getAllDepartments() {

		try {

			String sql = "SELECT * FROM department";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Department> departments = new ArrayList<Department>();

			while (rs.next()) {

				Department d = new Department();
				d.setId(rs.getInt("ID"));
				d.setName(rs.getString("NAME"));
				d.setHeadId (rs.getInt("HEAD_ID"));
				
				departments.add(d);
			}

			return departments;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	private int getNextIdentifier() {
		try {
			String sql = "SELECT department_seq.nextval FROM dual";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
		}
		return 0;
	}

	public Department addDepartment(Department d) {
		try {
			int id = getNextIdentifier();
			String sql = "INSERT INTO department (id, name, head_id) VALUES (?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setString(2, d.getName());
			ps.setInt(3, d.getHeadId());
			

			ps.executeQuery();
			return new Department(id, d.getName(), d.getHeadId());

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean updateDepartment(Department change) {

		try {

			String sql = "UPDATE department SET name = ?, head_id = ?  WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, change.getName());
			ps.setInt(2, change.getHeadId());
			ps.setInt(3, change.getId());
	
			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteDepartment(int id) {

		try {

			String sql = "DELETE department WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
