package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.Employee;
import models.Login;
import util.JDBCConnection;

public class LoginDAOImpl implements LoginDAO {
	public static Connection conn = JDBCConnection.getConnection();
	
	public Login getLogin(int id) {
		try {

			String sql = "SELECT * FROM login WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, Integer.toString(id));

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Login l = new Login(rs.getInt("ID"),rs.getString("USERNAME"),rs.getString("PASSWORD"),rs.getInt("EMPLOYEE_ID"));
				 return l;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	

	public List<Login> getAllLogin() {
		try {

			String sql = "SELECT * FROM login";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			List<Login> LoginList = new ArrayList<Login>();

			while (rs.next()) {

				Login l = new Login();
				l.setId(rs.getInt("ID"));
				l.setUsername(rs.getString("USERNAME"));

				LoginList.add(l);
			}

			return LoginList;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	private int getNextIdentifier() {
		try {
			String sql = "SELECT login_seq.nextval FROM dual";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
		}
		return 0;
	}
	

	public Login addLogin(Login l) {
		try {
			int id = getNextIdentifier();

			String sql = "INSERT INTO login (id, username, password, employee_id) VALUES (?,?,?,?)";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setString(2, l.getUsername());
			ps.setString(3, l.getPassword());
			ps.setInt(4, l.getEmployee_id());
			ps.executeQuery();
			return new Login(id, l.getUsername(), l.getPassword(), l.getEmployee_id());


		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public boolean updateLogin(Login change) {
		try {

			String sql = "UPDATE login SET username = ?, password =?, employee_id =? WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, change.getUsername());
			ps.setString(2, change.getPassword());
			ps.setInt(3, change.getEmployee_id());
			ps.setInt(4, change.getId());
			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteLogin(int id) {
		try {

			String sql = "DELETE login WHERE id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, Integer.toString(id));

			ps.executeQuery();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public Employee doLogin(String username, String password) {
	 try {
		 String sql = "SELECT employee.* FROM employee INNER JOIN login ON employee.id = login.employee_id WHERE login.username =? AND login.password =? ";
		 PreparedStatement ps = conn.prepareStatement(sql);

			ps.setString(1, username);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				Employee em = new Employee(rs.getInt("ID"), rs.getString("NAME"), rs.getString("LAST_NAME"), rs.getString("EMPLOYEE_TYPE"), rs.getInt("SUPERVISOR_ID"), rs.getInt("DEPARTMENT_ID"));
				 return em;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
