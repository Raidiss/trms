package models;

public class Employee {
	private int id;
	private String name;
	private String lastName;
	private String employeeType;
	private int supervisorId;
	private int departmentId;
	
	public Employee(String name, String lastName, String employeeType, int supervisorId, int departmentId) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.employeeType = employeeType;
		this.supervisorId = supervisorId;
		this.departmentId = departmentId;
	}

	public Employee(int id, String name, String lastName, String employeeType, int supervisorId, int departmentId) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.employeeType = employeeType;
		this.supervisorId = supervisorId;
		this.departmentId = departmentId;
	}

	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public int getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(int supervisorId) {
		this.supervisorId = supervisorId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", lastName=" + lastName + ", employeeType=" + employeeType
				+ ", supervisorId=" + supervisorId + ", departmentId=" + departmentId + "]";
	}
		
}
