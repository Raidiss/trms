package models;

public class Login {
	private int id;
	private String username;
	private String password;
	private int employee_id;
	public Login(int id, String username, String password, int employee_id) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.employee_id = employee_id;
	}
	public Login() {
		super();
	}
	public Login(String username, String password, int employee_id) {
		super();
		this.username = username;
		this.password = password;
		this.employee_id = employee_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	
	public String toString() {
		return "Login [id=" + id + ", username=" + username + ", password=" + password + ", employee_id=" + employee_id
				+ "]";
	}
	
	
}
