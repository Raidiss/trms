package models;

public class Reimbursement {
	private int id;
	private int employeeId;
	private int gradingId;
	private String startDate;
	private String time;
	private String location;
	private String description;
	private int cost;
	private String eventType;
	private String workRelation;
	private String approvedBySupervisor;
	private String approvedByHead;
	private String approvedByBenco;
	private String notes;
	
	public Reimbursement() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Reimbursement(int employeeId, int gradingId, String startDate, String time, String location,
			String description, int cost, String eventType, String workRelation, String approvedBySupervisor,
			String approvedByHead, String approvedByBenco, String notes) {
		super();
		this.employeeId = employeeId;
		this.gradingId = gradingId;
		this.startDate = startDate;
		this.time = time;
		this.location = location;
		this.description = description;
		this.cost = cost;
		this.eventType = eventType;
		this.workRelation = workRelation;
		this.approvedBySupervisor = approvedBySupervisor;
		this.approvedByHead = approvedByHead;
		this.approvedByBenco = approvedByBenco;
		this.notes = notes;
	}


	public Reimbursement(int id, int employeeId, int gradingId, String startDate, String time, String location,
			String description, int cost, String eventType, String workRelation, String approvedBySupervisor,
			String approvedByHead, String approvedByBenco, String notes) {
		super();
		this.id = id;
		this.employeeId = employeeId;
		this.gradingId = gradingId;
		this.startDate = startDate;
		this.time = time;
		this.location = location;
		this.description = description;
		this.cost = cost;
		this.eventType = eventType;
		this.workRelation = workRelation;
		this.approvedBySupervisor = approvedBySupervisor;
		this.approvedByHead = approvedByHead;
		this.approvedByBenco = approvedByBenco;
		this.notes = notes;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}


	public int getGradingId() {
		return gradingId;
	}


	public void setGradingId(int gradingId) {
		this.gradingId = gradingId;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getTime() {
		return time;
	}


	public void setTime(String time) {
		this.time = time;
	}

	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getCost() {
		return cost;
	}


	public void setCost(int cost) {
		this.cost = cost;
	}


	public String getEventType() {
		return eventType;
	}


	public void setEventType(String eventType) {
		this.eventType = eventType;
	}


	public String getWorkRelation() {
		return workRelation;
	}


	public void setWorkRelation(String workRelation) {
		this.workRelation = workRelation;
	}


	public String getApprovedBySupervisor() {
		return approvedBySupervisor;
	}


	public void setApprovedBySupervisor(String approvedBySupervisor) {
		this.approvedBySupervisor = approvedBySupervisor;
	}


	public String getApprovedByHead() {
		return approvedByHead;
	}


	public void setApprovedByHead(String approvedByHead) {
		this.approvedByHead = approvedByHead;
	}


	public String getApprovedByBenco() {
		return approvedByBenco;
	}


	public void setApprovedByBenco(String approvedByBenco) {
		this.approvedByBenco = approvedByBenco;
	}


	@Override
	public String toString() {
		return "Reimbursement [id=" + id + ", employeeId=" + employeeId + ", gradingId=" + gradingId + ", startDate="
				+ startDate + ", time=" + time + ", location=" + location + ", description=" + description + ", cost="
				+ cost + ", eventType=" + eventType + ", workRelation=" + workRelation + ", approvedBySupervisor="
				+ approvedBySupervisor + ", approvedByHead=" + approvedByHead + ", approvedByBenco=" + approvedByBenco
				+ ", notes=" + notes + "]";
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
