package models;

public class Grading {
	private int id;
	private String value;
	public Grading(int id, String value) {
		super();
		this.id = id;
		this.value = value;
	}
	public Grading(String value) {
		super();
		this.value = value;
	}
	public Grading() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Grading [id=" + id + ", value=" + value + "]";
	}

}
