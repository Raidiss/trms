package models;

public class Department {
	private int id;
	private String name;
	private int headId;
	
	public Department(String name, int headId) {
		super();
		this.name = name;
		this.headId = headId;
	}
	public Department(int id, String name, int headId) {
		super();
		this.id = id;
		this.name = name;
		this.headId = headId;
	}
	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getHeadId() {
		return headId;
	}
	public void setHeadId(int headId) {
		this.headId = headId;
	}
	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", headId=" + headId + "]";
	}
	
}
